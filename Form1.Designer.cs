﻿
namespace Toys
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Label_dispInfo = new System.Windows.Forms.Label();
            this.CB_ClassSelector = new System.Windows.Forms.ComboBox();
            this.B_AddClassObject = new System.Windows.Forms.Button();
            this.TB_GetObjectName = new System.Windows.Forms.TextBox();
            this.L_TextBoxLabel = new System.Windows.Forms.Label();
            this.L_typeObjSpeed = new System.Windows.Forms.Label();
            this.L_typeObjDepth = new System.Windows.Forms.Label();
            this.L_typeObjHeight = new System.Windows.Forms.Label();
            this.TB_GetObjectHeight = new System.Windows.Forms.TextBox();
            this.TB_GetObjectDepth = new System.Windows.Forms.TextBox();
            this.TB_GetObjectSpeed = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.B_UpdateObject = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_dispInfo
            // 
            this.Label_dispInfo.AutoSize = true;
            this.Label_dispInfo.Location = new System.Drawing.Point(209, 19);
            this.Label_dispInfo.Name = "Label_dispInfo";
            this.Label_dispInfo.Size = new System.Drawing.Size(31, 13);
            this.Label_dispInfo.TabIndex = 0;
            this.Label_dispInfo.Text = "Hello";
            this.Label_dispInfo.Click += new System.EventHandler(this.Label_dispInfo_Click);
            // 
            // CB_ClassSelector
            // 
            this.CB_ClassSelector.FormattingEnabled = true;
            this.CB_ClassSelector.Items.AddRange(new object[] {
            "Car",
            "Submarine",
            "Plane",
            "Computer"});
            this.CB_ClassSelector.Location = new System.Drawing.Point(20, 60);
            this.CB_ClassSelector.Name = "CB_ClassSelector";
            this.CB_ClassSelector.Size = new System.Drawing.Size(164, 21);
            this.CB_ClassSelector.TabIndex = 1;
            this.CB_ClassSelector.SelectedIndexChanged += new System.EventHandler(this.CB_ClassSelector_SelectedIndexChanged);
            // 
            // B_AddClassObject
            // 
            this.B_AddClassObject.Location = new System.Drawing.Point(30, 288);
            this.B_AddClassObject.Name = "B_AddClassObject";
            this.B_AddClassObject.Size = new System.Drawing.Size(75, 23);
            this.B_AddClassObject.TabIndex = 2;
            this.B_AddClassObject.Text = "Add Object";
            this.B_AddClassObject.UseVisualStyleBackColor = true;
            this.B_AddClassObject.Click += new System.EventHandler(this.B_AddClassObject_Click);
            // 
            // TB_GetObjectName
            // 
            this.TB_GetObjectName.Location = new System.Drawing.Point(20, 136);
            this.TB_GetObjectName.Name = "TB_GetObjectName";
            this.TB_GetObjectName.Size = new System.Drawing.Size(164, 20);
            this.TB_GetObjectName.TabIndex = 3;
            this.TB_GetObjectName.TextChanged += new System.EventHandler(this.TB_GetObjectName_TextChanged);
            // 
            // L_TextBoxLabel
            // 
            this.L_TextBoxLabel.AutoSize = true;
            this.L_TextBoxLabel.Location = new System.Drawing.Point(27, 120);
            this.L_TextBoxLabel.Name = "L_TextBoxLabel";
            this.L_TextBoxLabel.Size = new System.Drawing.Size(149, 13);
            this.L_TextBoxLabel.TabIndex = 4;
            this.L_TextBoxLabel.Text = "Type in your new object name";
            // 
            // L_typeObjSpeed
            // 
            this.L_typeObjSpeed.AutoSize = true;
            this.L_typeObjSpeed.Location = new System.Drawing.Point(27, 159);
            this.L_typeObjSpeed.Name = "L_typeObjSpeed";
            this.L_typeObjSpeed.Size = new System.Drawing.Size(111, 13);
            this.L_typeObjSpeed.TabIndex = 8;
            this.L_typeObjSpeed.Text = "Type in objects speed";
            // 
            // L_typeObjDepth
            // 
            this.L_typeObjDepth.AutoSize = true;
            this.L_typeObjDepth.Location = new System.Drawing.Point(27, 202);
            this.L_typeObjDepth.Name = "L_typeObjDepth";
            this.L_typeObjDepth.Size = new System.Drawing.Size(109, 13);
            this.L_typeObjDepth.TabIndex = 9;
            this.L_typeObjDepth.Text = "Type in objects depth";
            // 
            // L_typeObjHeight
            // 
            this.L_typeObjHeight.AutoSize = true;
            this.L_typeObjHeight.Location = new System.Drawing.Point(27, 246);
            this.L_typeObjHeight.Name = "L_typeObjHeight";
            this.L_typeObjHeight.Size = new System.Drawing.Size(113, 13);
            this.L_typeObjHeight.TabIndex = 10;
            this.L_typeObjHeight.Text = "Type in objects Height";
            // 
            // TB_GetObjectHeight
            // 
            this.TB_GetObjectHeight.Location = new System.Drawing.Point(20, 262);
            this.TB_GetObjectHeight.Name = "TB_GetObjectHeight";
            this.TB_GetObjectHeight.Size = new System.Drawing.Size(164, 20);
            this.TB_GetObjectHeight.TabIndex = 14;
            this.TB_GetObjectHeight.TextChanged += new System.EventHandler(this.TB_GetObjectHeight_TextChanged);
            // 
            // TB_GetObjectDepth
            // 
            this.TB_GetObjectDepth.Location = new System.Drawing.Point(20, 218);
            this.TB_GetObjectDepth.Name = "TB_GetObjectDepth";
            this.TB_GetObjectDepth.Size = new System.Drawing.Size(164, 20);
            this.TB_GetObjectDepth.TabIndex = 13;
            this.TB_GetObjectDepth.TextChanged += new System.EventHandler(this.TB_GetObjectDepth_TextChanged);
            // 
            // TB_GetObjectSpeed
            // 
            this.TB_GetObjectSpeed.Location = new System.Drawing.Point(20, 175);
            this.TB_GetObjectSpeed.Name = "TB_GetObjectSpeed";
            this.TB_GetObjectSpeed.Size = new System.Drawing.Size(164, 20);
            this.TB_GetObjectSpeed.TabIndex = 12;
            this.TB_GetObjectSpeed.TextChanged += new System.EventHandler(this.TB_GetObjectSpeed_TextChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(270, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(533, 374);
            this.dataGridView1.TabIndex = 15;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // B_UpdateObject
            // 
            this.B_UpdateObject.Location = new System.Drawing.Point(130, 288);
            this.B_UpdateObject.Name = "B_UpdateObject";
            this.B_UpdateObject.Size = new System.Drawing.Size(93, 23);
            this.B_UpdateObject.TabIndex = 16;
            this.B_UpdateObject.Text = "Update Object";
            this.B_UpdateObject.UseVisualStyleBackColor = true;
            this.B_UpdateObject.Click += new System.EventHandler(this.B_UpdateObject_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 405);
            this.Controls.Add(this.B_UpdateObject);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.TB_GetObjectHeight);
            this.Controls.Add(this.TB_GetObjectDepth);
            this.Controls.Add(this.TB_GetObjectSpeed);
            this.Controls.Add(this.L_typeObjHeight);
            this.Controls.Add(this.L_typeObjDepth);
            this.Controls.Add(this.L_typeObjSpeed);
            this.Controls.Add(this.L_TextBoxLabel);
            this.Controls.Add(this.TB_GetObjectName);
            this.Controls.Add(this.B_AddClassObject);
            this.Controls.Add(this.CB_ClassSelector);
            this.Controls.Add(this.Label_dispInfo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_dispInfo;
        private System.Windows.Forms.ComboBox CB_ClassSelector;
        private System.Windows.Forms.Button B_AddClassObject;
        private System.Windows.Forms.TextBox TB_GetObjectName;
        private System.Windows.Forms.Label L_TextBoxLabel;
        private System.Windows.Forms.Label L_typeObjSpeed;
        private System.Windows.Forms.Label L_typeObjDepth;
        private System.Windows.Forms.Label L_typeObjHeight;
        private System.Windows.Forms.TextBox TB_GetObjectHeight;
        private System.Windows.Forms.TextBox TB_GetObjectDepth;
        private System.Windows.Forms.TextBox TB_GetObjectSpeed;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button B_UpdateObject;
    }
}

