using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toys.Toys
{
    public class Plane : ToyBase, IAccelerate, IRise
    {
        int planeSpeed, planeAlt;

        public Plane(string name, int speed, int height) : base(name)
        {
            Speed = speed;
            Height = height;
        }

        public void Accelerate(int speed) => planeSpeed = speed;

        public void Rise(int height) => planeAlt = height;

    }
}
