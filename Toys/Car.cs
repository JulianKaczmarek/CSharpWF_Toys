﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toys.Toys
{
    public class Car : ToyBase, IAccelerate
    {
        public Car(string name, int speed) : base(name)
        {
            Speed = speed;
        }
        public void Accelerate(int speed) => Speed = speed;
    }
}
