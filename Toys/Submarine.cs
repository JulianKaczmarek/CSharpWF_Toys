using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toys.Toys
{
    public class Submarine : ToyBase, IDive, IAccelerate
    {
        int subSpeed, subDepth;

        public Submarine(string name, int speed, int depth) : base(name)
        {
            Speed = speed;
            Depth = depth;
        }

        public void Accelerate(int speed) => subSpeed = speed;

        public void IDive(int depth) => subDepth = depth;

    }
}
