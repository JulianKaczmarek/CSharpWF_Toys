﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toys.Toys
{
    public class ToyBase
    {
        public ToyBase(string name)
        {
            Type = this.GetType().Name;
            Name = name;
        }

        public string Type { get; set; }
        public string  Name { get; set; }
        public int? Speed { get; set; } //? nullable
        public int? Depth { get; set; }
        public int? Height { get; set; }
    }
}
