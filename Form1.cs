﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toys.Toys;

namespace Toys
{
    public partial class Form1 : Form
    {
        BindingList<ToyBase> toyBases;
        public Form1()
        {
            InitializeComponent();
            toyBases = new BindingList<ToyBase>();
            CB_ClassSelector.SelectedIndex = 0;
            dataGridView1.DataSource = toyBases;
            dataGridView1.AutoGenerateColumns = true;

            // Event Assign C#
            TB_GetObjectDepth.Validating += validateingInt;
            TB_GetObjectSpeed.Validating += validateingInt;
            TB_GetObjectHeight.Validating += validateingInt;
        }

        private void validateingInt(object sender, CancelEventArgs e)
        {
            var textBox = sender as TextBox;
            var text = textBox.Text;
            int value;

            errorProvider1.Clear();
            e.Cancel = false;

            if (text == "Type here")
            {
                return;
            }

            if (int.TryParse(text, out value))
            {
                return;
            }
            e.Cancel = true;
            errorProvider1.SetError(sender as Control, "Data format error");
        }

        private void B_AddClassObject_Click(object sender, EventArgs e)
        {
            string cbSelection = CB_ClassSelector.SelectedItem.ToString();
            var toyName = TB_GetObjectName.Text;

            var toySpeed = int.Parse(TB_GetObjectSpeed.Text == "Type here"? "0" : TB_GetObjectSpeed.Text);
            var toyDepth = int.Parse(TB_GetObjectDepth.Text == "Type here" ? "0" : TB_GetObjectDepth.Text);
            var toyHeight = int.Parse(TB_GetObjectHeight.Text == "Type here" ? "0" : TB_GetObjectHeight.Text);

            switch (cbSelection)
            {
                case "Car":
                    toyBases.Add(new Car(toyName, toySpeed));
                    break;
                case "Computer":
                    toyBases.Add(new Computer(toyName));
                    break;
                case "Plane":
                    toyBases.Add(new Plane(toyName, toySpeed, toyHeight));
                    break;
                case "Submarine":
                    toyBases.Add(new Submarine(toyName, toySpeed, toyDepth));
                    break;
            }
        }
        
        private void CB_ClassSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cbSelection = CB_ClassSelector.SelectedItem.ToString();
            TB_GetObjectHeight.Text = "Type here";
            TB_GetObjectSpeed.Text = "Type here";
            TB_GetObjectDepth.Text = "Type here";
            TB_GetObjectName.Text = "Nazwa";

            switch (cbSelection)
            {
                case "Car":
                    L_typeObjHeight.Visible = false;
                    TB_GetObjectHeight.Visible = false;

                    L_typeObjSpeed.Visible = true;
                    TB_GetObjectSpeed.Visible = true;

                    L_typeObjDepth.Visible = false;
                    TB_GetObjectDepth.Visible = false;
                    break;
                case "Computer":
                    L_typeObjHeight.Visible = false;
                    TB_GetObjectHeight.Visible = false;

                    L_typeObjSpeed.Visible = false;
                    TB_GetObjectSpeed.Visible = false;

                    L_typeObjDepth.Visible = false;
                    TB_GetObjectDepth.Visible = false;
                    break;
                case "Plane":
                    L_typeObjHeight.Visible = true;
                    TB_GetObjectHeight.Visible = true;

                    L_typeObjSpeed.Visible = true;
                    TB_GetObjectSpeed.Visible = true;

                    L_typeObjDepth.Visible = false;
                    TB_GetObjectDepth.Visible = false;
                    break;
                case "Submarine":
                    L_typeObjHeight.Visible = false;
                    TB_GetObjectHeight.Visible = false;

                    L_typeObjSpeed.Visible = true;
                    TB_GetObjectSpeed.Visible = true;

                    L_typeObjDepth.Visible = true;
                    TB_GetObjectDepth.Visible = true;
                    break;
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var toy = toyBases[dataGridView1.SelectedRows[0].Index];
                L_typeObjSpeed.Visible = toy is IAccelerate;
                TB_GetObjectSpeed.Visible = toy is IAccelerate;

                L_typeObjDepth.Visible = toy is IDive;
                TB_GetObjectDepth.Visible = toy is IDive;

                L_typeObjHeight.Visible = toy is IRise;
                TB_GetObjectHeight.Visible = toy is IRise;
            }
        }

        private void B_UpdateObject_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                //TB_GetObjectSpeed.Clear();
                //TB_GetObjectDepth.Clear();
                //TB_GetObjectHeight.Clear();
                var toy = toyBases[dataGridView1.SelectedRows[0].Index];
                var toySpeed = toy as IAccelerate;
                var toyHeight = toy as IRise;
                var toyDepth = toy as IDive;
          
                if (toySpeed != null)
                {
                    var speed = int.Parse(TB_GetObjectSpeed.Text == "Type here" ? "0" : TB_GetObjectSpeed.Text);
                    toySpeed.Accelerate(speed);
                    dataGridView1.Refresh();
                }
                if (toyHeight != null)
                {
                    var height = int.Parse(TB_GetObjectHeight.Text == "Type here" ? "0" : TB_GetObjectHeight.Text);
                    toyHeight.Rise(height);
                    dataGridView1.Refresh();
                }
                if (toyDepth != null)
                {
                    var depth = int.Parse(TB_GetObjectDepth.Text == "Type here" ? "0" : TB_GetObjectDepth.Text);
                    toyDepth.IDive(depth);
                    dataGridView1.Refresh();
                }
                
            }
        }
        private void Label_dispInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Nothing to do here");
        }
        private void TB_GetObjectName_TextChanged(object sender, EventArgs e)
        {

        }

        private void TB_GetObjectSpeed_TextChanged(object sender, EventArgs e)
        {

        }

        private void TB_GetObjectDepth_TextChanged(object sender, EventArgs e)
        {

        }

        private void TB_GetObjectHeight_TextChanged(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        
    }
}
